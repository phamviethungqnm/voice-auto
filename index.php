
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Text to speech s3</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="shortcut icon" href="#">
        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    </head>
    <body>
        <div class="container text-center">
            <h1>Text to speech s3</h1>
            <div class="form-group">
                <form action="" mothod="post">
                    <div class="mb-3">
                        <textarea name="textarea" class="form-control w-50 mx-auto" id="text-form" rows="5" placeholder="Text area..."></textarea>
                    </div>
                    <div class="form-group d-flex justify-content-center">
                        <button type="button" id="btn-submit" class="btn btn-primary me-3">convert to Mp3</button>
                        <select class="form-select w-25 me-3" id="form-select">
                            <option value="Joanna">English(US)</option>
                            <option value="Nicole">English(AU)</option>
                            <option value="Emma">English(GB)</option>
                            <option value="Raveena">English(IN)</option>
                            <option value="Aria">English(NZ)</option>
                            <option value="Ayanda">English(ZA)</option>
                            <option value="Geraint">English(WLS)</option>
                            <option value="Mizuki">Japan</option>
                            <option value="Zhiyu">Chinese</option>
                            <option value="Seoyeon">Korean</option>
                            <option value="Zeina">Arabic</option>
                            <option value="Naja">Danish</option>
                            <option value="Lotte">Dutch</option>
                            <option value="Mathieu">French</option>
                            <option value="Vicki">German</option>
                            <option value="Aditi">Hindi</option>
                            <option value="Karl">Iceland</option>
                            <option value="Carla">Italian</option>
                            <option value="Liv">Norwegian</option>
                            <option value="Ewa">Polish</option>
                            <option value="Camila">Portuguese</option>
                            <option value="Carmen">Romanian</option>
                            <option value="Maxim">Russian</option>
                            <option value="Conchita">Spanish</option>
                            <option value="Astrid">Swedish</option>
                            <option value="Filiz">Turkish</option>
                            <option value="Gwyneth">Welsh</option>
                        </select>
                        <button type="button" class="btn btn-warning" id="clear-text">Clear text</button>
                    </div>
                </form>
            </div>
            <div class="result mt-5" id="result"></div>
        </div>
        <script>
            $(document).ready(function() {
                // show loading... 
                $(document).ajaxStart(function() {
                    swal.showLoading();
                });
                $(document).ajaxComplete(function() {
                    swal.close();
                });

                // convert text to speech
                $(document).on('click', '#btn-submit', function() {
                    const valueText = $('#text-form').val();
                    const valueSelect = $('#form-select').val();
                    $.get({
                        url: "data.php",
                        dataType: "text",
                        data: {
                            'pushvalue': 1,
                            'value': valueText,
                            'voice': valueSelect
                        },
                        success: function(data) {
                            $.get(data).done(function() {
                                // console.log(data)
                                setTimeout(function(){ showaudio(data) }, 500);
                                
                            }).fail(function() {
                                console.log('error')
                            })
                        }
                    });
                })
                
                // show audio mp3
                function showaudio(data) {
                    var audio = `
                        <audio id="" controls>
                            <source id="audiosource" src="${data}" type="audio/mp3">
                        </audio>
                    `;
                    $('#result').html(audio);
                }

                // clear textarea
                $(document).on('click', '#clear-text', function() {
                    $('#text-form').val('');
                })
            })
        </script>
    </body>
</html>
